
 # 继承centos镜像
 FROM centos
 # 运行 echo 命令，输出 “hello dockerfile” 字符串
 RUN echo "Hello Dockerfile."
 # 容器启动时执行该命令
 CMD /bin/bash
